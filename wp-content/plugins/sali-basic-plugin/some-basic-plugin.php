<?php
	/*
		Plugin Name: Sali Basic Plugin
		Version: 0.1.0
		Description: Demo Plugin Development 
		Author: Sali Safi
	*/
		
	add_action('admin_notices','sali_basic_plugin_show_activated');
	
	function sali_basic_plugin_show_activated()
	{
?>
		<div class="notice notice-success">
			<p>Dear Salimeh, Sali's basic Plugin is activated!</p>
		</div>
<?php
	}
?>