=== Shopkeeper Extender ===
Contributors: getbowtied, vanesareinerth, adrianlbs
Tags: gutenberg, blocks
Requires at least: 5.0
Tested up to: 5.0.3
Stable tag: 1.3.3
Requires PHP: 5.5.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
~Current Version:1.3.3~

Extends the functionality of the Shopkeeper theme by adding theme specific features.

== Description ==

Companion plugin for the **Shopkeeper** theme. Extends the functionality by adding theme specific features.

**Gutenberg Blocks:**
- Image Slider
- Banner
- Posts Grid
- Product Categories Grid
- Portfolio
- Social Media Profiles

== Changelog ==

= 1.3.3 =
- Fixed: Columns Block displaying issue
- Fixed: Yoast SEO compatibiity issue

= 1.3.2 =
- Fixed: Localization issues with the Posts Grid Block

= 1.3.1 =
- WordPress 5+ compatibility improvements
- Improved styles for theme neutrality 

= 1.3 =
- WordPress 5+ compatibility improvements

= 1.2 =
- Improvement: Compatibility updates for Gutenberg 4.0.0

= 1.1 =
- New: Product Count on Product Categories Grid

= 1.0 =
- Initial Version
